REGISTRY ?= quay.io
ORG ?= mergetb
TAG ?= latest
IMG := $(REGISTRY)/$(ORG)/user-ui:$(TAG)

.PHONY: user-ui-ctr
user-ui-ctr:
	docker build --no-cache -t $(IMG) .
	$(if ${PUSH}, docker push $(IMG))

